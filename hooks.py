# -*- coding: utf-8 -*-

import bpy
import os
import sys
import math
import webbrowser

k=0.01

TRANSFORMS={
    'tx': 'location[0]',
    'ty': 'location[1]',
    'tz': 'location[2]',
    'rx': 'rotation_euler[0]',
    'ry': 'rotation_euler[1]', 
    'rz': 'rotation_euler[2]',
    'sx': 'scale[0]',
    'sy': 'scale[1]',
    'sz': 'scale[2]',
}

# GEN_SHAPE_KEYS={
#     'lipFunnel':['lipFunnelUp','lipFunnelLw']
# }

def _get_weight(x, min, max):
        value= (x-min)/(max-min)
        return value

def make_vertex_groups(context, action):
    border_name = f'{action}_border'
    l_group_name=f'{action}_left'
    r_group_name=f'{action}_right'

    # (0) get mesh
    ob = context.object
    if ob.type != 'MESH':
        return (False, 'the selected object is not a MESH!')

    # (1) border exists
    if border_name in bpy.data.objects:
        border = bpy.data.objects[border_name]
    else:
        return (False, f'missing object {border_name}')

    # (2) vertex groups exists
    if not l_group_name in ob.vertex_groups:
        l_v_group=ob.vertex_groups.new(name=l_group_name)
    else:
        l_v_group=ob.vertex_groups[l_group_name]
    # --
    if not r_group_name in ob.vertex_groups:
        r_v_group=ob.vertex_groups.new(name=r_group_name)
    else:
        r_v_group=ob.vertex_groups[r_group_name]

    # (3) weights
    left_border = border.location[0]
    right_border = -left_border
    # -- l
    for v in ob.data.vertices:
        if v.co[0]*k>left_border:
            l_v_group.add([v.index], 1.0, 'REPLACE')
            r_v_group.add([v.index], 0.0, 'REPLACE')
        elif v.co[0]*k<right_border:
            l_v_group.add([v.index], 0.0, 'REPLACE')
            r_v_group.add([v.index], 1.0, 'REPLACE')
        else:
            value=_get_weight(v.co[0]*k, right_border, left_border)
            l_v_group.add([v.index], value, 'REPLACE')
            r_v_group.add([v.index], 1-value, 'REPLACE')
    # # -- r
    # for v in ob.data.vertices:
    #     if v.co[0]*k<left_border:
    #         r_v_group.add([v.index], 1.0, 'REPLACE')
    #     elif v.co[0]*k>right_border:
    #         r_v_group.add([v.index], 0.0, 'REPLACE')
    #     else:
    #         value=_get_weight(v.co[0]*k, right_border, left_border)
    #         r_v_group.add([v.index], 1-value, 'REPLACE')

    return (True, f'Ok!')

def make_brows_vertex_groups(context):
    OUTER_BORDER_NAME = 'outer_border'
    INNER_BORDER_NAME = 'inner_border'
    OUTER_GROUP_NAME = 'brows_outer'
    INNER_GROUP_NAME = 'brows_inner'

    # get mesh
    ob = context.object
    assert ob.type == 'MESH'

    # get borders
    if OUTER_BORDER_NAME in bpy.data.objects:
        outer_border = bpy.data.objects[OUTER_BORDER_NAME]
    else:
        return False, 'Not Found %s' % OUTER_BORDER_NAME
    if INNER_BORDER_NAME in bpy.data.objects:
        inner_border = bpy.data.objects[INNER_BORDER_NAME]
    else:
        return False, 'Not Found %s' % INNER_BORDER_NAME

    inner = inner_border.location[0]
    outer = outer_border.location[0]
    
    # vertex_groups test exists
    # --
    if not OUTER_GROUP_NAME in ob.vertex_groups:
        outer_v_group=ob.vertex_groups.new(name=OUTER_GROUP_NAME)
    else:
        outer_v_group=ob.vertex_groups[OUTER_GROUP_NAME]
    # --
    if not INNER_GROUP_NAME in ob.vertex_groups:
        inner_v_group=ob.vertex_groups.new(name=INNER_GROUP_NAME)
    else:
        inner_v_group=ob.vertex_groups[INNER_GROUP_NAME]

    # weights
    # -- inner
    for v in ob.data.vertices:
        if abs(v.co[0]*k)>outer:
            inner_v_group.add([v.index], 0.0, 'REPLACE')
        elif abs(v.co[0]*k)<inner:
            inner_v_group.add([v.index], 1.0, 'REPLACE')
        else:
            value=_get_weight(abs(v.co[0])*k, inner, outer)
            inner_v_group.add([v.index], 1-value, 'REPLACE')

    # -- outer
    for v in ob.data.vertices:
        if abs(v.co[0]*k)>outer:
            outer_v_group.add([v.index], 1.0, 'REPLACE')
        elif abs(v.co[0]*k)<inner:
            outer_v_group.add([v.index], 0.0, 'REPLACE')
        else:
            value=_get_weight(abs(v.co[0])*k, inner, outer)
            outer_v_group.add([v.index], value, 'REPLACE')

    return True, 'Ok!'

def set_current_pose(context, poses_data, pose):
    for key in poses_data[pose]:
        name=key.split('.')[0]
        attr=key.split('.')[-1:][0]
        value=poses_data[pose][key]
        print('*** %s' % name)
        print(poses_data[pose])
        
        #search ob
        ob=False
        if name in bpy.data.objects:
            ob=bpy.data.objects[name]
        else:
            for item in bpy.data.objects:
                if item.type=='ARMATURE':
                    if name in item.pose.bones:
                        context.view_layer.objects.active = item
                        bpy.ops.object.mode_set(mode='POSE')
                        ob=item.pose.bones[name]
                        ob.rotation_mode='XYZ'
                        print('*** %s' % name)
                        break
            
        if not ob:
            return 'objects (%s) not found' % name
            
        #set attr
        if attr in TRANSFORMS:
            if attr.startswith('r'):
                value = value*math.pi/180
            com='ob.%s=%s' % (TRANSFORMS[attr], value)
            print('*** %s' % com)
            exec(com)
        else:
            return 'not found attribute: %s' % attr
        
        bpy.ops.object.mode_set(mode='OBJECT')
        
    return 'set pose:'

def clear_shape_keys(ob, gen_shape_keys):
    for source, gen_keys in gen_shape_keys.items():
        for item in gen_keys:
            if isinstance(item, str):
                shk_name=item
            elif isinstance(item, tuple) or isinstance(item, list):
                shk_name=item[0]
            #
            if not shk_name in ob.data.shape_keys.key_blocks:
                continue
            else:
                shape_key = ob.data.shape_keys.key_blocks[shk_name]
                ob.shape_key_remove(shape_key)


def generate_shape_keys(context, gen_shape_keys, iter=0):
    """generate shape keys from selected mesh."""

    if iter>3:
        return(False, "Over iteration: %s" % gen_shape_keys.keys())

    # get mesh
    ob = context.object
    if ob.type != 'MESH':
        return False, 'The selected object is not a MESH!'

    if iter==0:
        clear_shape_keys(ob, gen_shape_keys)
    # return True, 'clear'

    # make shape keys
    second_stage={}
    generated_shape_keys=[]
    for source, gen_keys in gen_shape_keys.items():
        if not source in ob.data.shape_keys.key_blocks:
            # return False, 'Source Shape_key("%s") not found!' % source
            second_stage[source]=gen_keys
            continue
        source = ob.data.shape_keys.key_blocks[source]

        # -- append shape keys
        for item in gen_keys:
            if isinstance(item, str):
                vtx_name=shk_name=item
            elif isinstance(item, tuple) or isinstance(item, list):
                shk_name=item[0]
                vtx_name=item[1]
            else:
                return False, f'wrong type of "item" {type(item)}'
            if not vtx_name in ob.vertex_groups:
                return False, 'Object has no vertex group %s!' % vtx_name
            vtx_group = ob.vertex_groups[vtx_name]
            if not shk_name in ob.data.shape_keys.key_blocks:
                shape_key = ob.shape_key_add(name=shk_name, from_mix=False)
            else:
                shape_key = ob.data.shape_keys.key_blocks[shk_name]
            basis = shape_key.relative_key
            print("basis: %s" % basis.name)
            print("basis: %s" % shape_key.name)
            generated_shape_keys.append(shk_name)

            for vtx in ob.data.vertices:
                try:
                    f = vtx_group.weight(vtx.index)
                except Exception as e:
                    #print("%s - %s" % (e, vtx.index))
                    continue
                else:
                    pass
                    # print(vtx.index)
                v = source.data[vtx.index].co[:]
                bv=basis.data[vtx.index].co[:]
                print(v, bv)
                for i in range(0, 3):
                    value = bv[i] + (v[i] - bv[i])*f
                    shape_key.data[vtx.index].co[i] = value

    if second_stage:
        return generate_shape_keys(context, second_stage, iter=iter+1)

    return True, ", ".join(generated_shape_keys)

def make_correct_shape_keys(context, corrects):
    # get mesh
    ob = context.object
    if ob.type != 'MESH':
        return False, 'The selected object is not a MESH!'

    for source_name, data in corrects.items():
        if not source_name in ob.data.shape_keys.key_blocks:
            continue
        source_shk=ob.data.shape_keys.key_blocks[source_name]
        pass


    return True, 'null'