# -*- coding: utf-8 -*-

import bpy
import os
import sys
import math
import webbrowser

from . import hooks

class G():
    poses_data = {}
    gen_shape_keys = {}

def get_lists():
    dir = os.path.dirname(bpy.data.filepath)
    # path = os.path.join(dir, 'bs_face_simple_local')
    path = dir
    # return os.path.exists(path)

    if not path in sys.path:
        sys.path.append(path)

    from bs_face_simple_local import lists

    G.poses_data = lists.POSES_DATA
    G.gen_shape_keys = lists.GEN_SHAPE_KEYS
    G.corrects=lists.CORRECTS

def help():
    path='https://gitlab.com/volodya-renderberg/bs_face_simple_data_to_blender/-/blob/master/README.md'
    webbrowser.open(path)

class poses_panel(bpy.types.Panel):
    bl_label = 'Set Poses:'
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = 'Face Poses'

    def draw(self, context):
        get_lists()

        layout = self.layout
        
        col = layout.column(align = True)
        col.operator('bsfacesimple.help_link', icon='HELP')
        for key in G.poses_data:
            #col.label(text=key)
            col.operator('bsfacesimple.set_pose', text=key).pose=key
        # col.operator('bsfacesimple.make_vertex_groups', icon='GROUP_VERTEX')
        # col.operator('bsfacesimple.unregister_panel', icon='CANCEL')

class shape_keys_generate_panel(bpy.types.Panel):
    bl_label = 'Shape Keys Generate:'
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = 'ShKeys Gen'

    def draw(self, context):
        layout = self.layout

        col = layout.column(align = True)
        col.operator('bsfacesimple.help_link', icon='HELP')

        col = layout.column(align = True)
        col.operator('bsfacesimple.make_vertex_groups', text="Brows vertex groups", icon='GROUP_VERTEX').action="brows"
        col.operator('bsfacesimple.make_vertex_groups', text="Lips vertex groups", icon='GROUP_VERTEX').action="lips"

        col = layout.column(align = True)
        col.operator('bsfacesimple.make_correct_shape_keys')
        col.operator('bsfacesimple.generate_shape_keys')
        
class set_pose(bpy.types.Operator):
    bl_idname = "bsfacesimple.set_pose"
    bl_label = "Set Pose"
    
    pose: bpy.props.StringProperty()

    def execute(self, context):
        if self.pose!='Neutral':
            r = hooks.set_current_pose(context, G.poses_data, 'Neutral')
            r = hooks.set_current_pose(context, G.poses_data, self.pose)
        else:
            r = hooks.set_current_pose(context, G.poses_data, self.pose)
        self.report({'INFO'}, '%s : %s' % (r, self.pose))
        return{'FINISHED'}

class make_vertex_groups(bpy.types.Operator):
    bl_idname = "bsfacesimple.make_vertex_groups"
    bl_label = "Make Vertex Groups"

    action: bpy.props.StringProperty()

    def execute(self, context):
        if self.action=='brows':
            b,r=hooks.make_brows_vertex_groups(context)
        elif self.action=='lips':
            b,r=hooks.make_vertex_groups(context, self.action)
        if b:
            self.report({'INFO'}, r)
        else:
            self.report({'WARNING'}, r)
        return{'FINISHED'}

class make_correct_shape_keys(bpy.types.Operator):
    bl_idname = "bsfacesimple.make_correct_shape_keys"
    bl_label = "Corrects Shape Keys"

    def execute(self, context):
        get_lists()

        b,r=hooks.make_correct_shape_keys(context, G.corrects)
        if b:
            self.report({'INFO'}, 'generated: %s' % r)
        else:
            self.report({'WARNING'}, r)

        return{'FINISHED'}

class generate_shape_keys(bpy.types.Operator):
    bl_idname = "bsfacesimple.generate_shape_keys"
    bl_label = "Generate Shape Keys"

    def execute(self, context):
        get_lists()
        
        b,r=hooks.generate_shape_keys(context, G.gen_shape_keys)
        if b:
            self.report({'INFO'}, 'generated: %s' % r)
        else:
            self.report({'WARNING'}, r)
        return{'FINISHED'}

class unregister_panel(bpy.types.Operator):
    bl_idname = "bsfacesimple.unregister_panel"
    bl_label = "Off Panel"

    def execute(self, context):
        unregister()
        return{'FINISHED'}

class help_link(bpy.types.Operator):
    bl_idname = "bsfacesimple.help_link"
    bl_label = "Help"

    def execute(self, context):
        help()
        return{'FINISHED'}

def register():
    bpy.utils.register_class(poses_panel)
    bpy.utils.register_class(shape_keys_generate_panel)
    bpy.utils.register_class(make_correct_shape_keys)
    bpy.utils.register_class(generate_shape_keys)
    bpy.utils.register_class(unregister_panel)
    bpy.utils.register_class(set_pose)
    bpy.utils.register_class(make_vertex_groups)
    bpy.utils.register_class(help_link)

def unregister():
    bpy.utils.unregister_class(poses_panel)
    bpy.utils.unregister_class(shape_keys_generate_panel)
    bpy.utils.unregister_class(make_correct_shape_keys)
    bpy.utils.unregister_class(generate_shape_keys)
    bpy.utils.unregister_class(unregister_panel)
    bpy.utils.unregister_class(set_pose)
    bpy.utils.unregister_class(make_vertex_groups)
    bpy.utils.unregister_class(help_link)

# register()