# bs_face_simple_data_to_blender

* [Постановка поз из lists.POSES_DATA](#%D0%BF%D0%BE%D1%81%D1%82%D0%B0%D0%BD%D0%BE%D0%B2%D0%BA%D0%B0-%D0%BF%D0%BE%D0%B7-%D0%B8%D0%B7-listsposes_data)
* [Генерация вертекс групп](#%D0%B3%D0%B5%D0%BD%D0%B5%D1%80%D0%B0%D1%86%D0%B8%D1%8F-%D0%B2%D0%B5%D1%80%D1%82%D0%B5%D0%BA%D1%81-%D0%B3%D1%80%D1%83%D0%BF%D0%BF)
* [Генерация Shape_keys](#%D0%B3%D0%B5%D0%BD%D0%B5%D1%80%D0%B0%D1%86%D0%B8%D1%8F-shape_keys)


Постановка поз из lists.POSES_DATA
----------------------------------

* Вкладка **Face Poses**



Генерация вертекс групп
-----------------------

Вкладка **Shkeys Gen**.

### Brows vertex groups

* Кнопка: ``Brows Vertex Groups``.
* Объекты обозначающие границы:
    * ``inner_border``
    * ``outer_border``
* Создаваемы вертекс группы:
    * ``brows_outer``
    * ``brows_inner``
    * ![image](/uploads/aec09f4174f9741d34a5e5cb22e59581/image.png)

### Lips vertex groups
* Кнопка: ``Lips vertex groups``.
* Объект обозначающий границу (располагать слевой стороны персонажа **+х** ):
    * ``lips_border``
* Создаваемые вертекс группы:
    * ``lips_left``
    * ``lips_right``


Генерация Shape_keys
--------------------

* Вкладка **Shkeys Gen**
* Кнопка ``Generate Shape Keys``
* создаются бленды с распилкой по вертекс группам по словарю ``bs_face_simple_local/lists.GEN_SHAPE_KEYS``
    * ключи словаря - имена исходников
    * значения - списки создаваемых форм, распилка идёт по одноимённым для создаваемых форм вертекс группам.
