#
#
#
#

bl_info = {
    "name": "Bs Face Simple Tools",
    "description": "Tools for Bs Face Simple.",
    "author": "Volodya Renderberg",
    "version": (1, 0),
    "blender": (2, 90, 1),
    "location": "View3d tools panel",
    "warning": "", # used for warning icon and text in addons panel
    "doc_url":"https://gitlab.com/volodya-renderberg/bs_face_simple_data_to_blender/-/blob/master/README.md",
    "category": "Rigging"}

if "bpy" in locals():
    import importlib
    importlib.reload(run)
else:
    from . import run

import bpy


##### REGISTER #####

def register():
    run.register()

def unregister():
    run.unregister()

if __name__ == "__main__":
    register()

